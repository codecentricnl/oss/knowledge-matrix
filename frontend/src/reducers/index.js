import { combineReducers } from 'redux';
import EmployeesReducer from "./EmployeesReducer";
import SkillsReducer from "./SkillsReducer";
import RatingsReducer from "./RatingsReducer";
import SkillCollectionsReducer from "./SkillCollectionsReducer";
import UserReducer from "./UserReducer";

const rootReducer = combineReducers({
    employees: EmployeesReducer,
    skills: SkillsReducer,
    ratings: RatingsReducer,
    skillCollections: SkillCollectionsReducer,
    currentUser: UserReducer
});

export default rootReducer;