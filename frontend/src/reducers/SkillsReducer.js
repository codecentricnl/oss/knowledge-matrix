import {SKILLS} from '../actions'
import _ from "lodash";
import {DELETE_SKILL, ADD_SKILL} from "../actions/index";

export default function (state = {}, action) {
    switch (action.type) {
        case SKILLS:
            return _.mapKeys(action.payload.data, 'id');
        case DELETE_SKILL:
            return _.omit(state, action.payload.data.id);
        case ADD_SKILL:
            return { ...state, [action.payload.data.id]: action.payload.data }
        default:
            return state;
    }
}