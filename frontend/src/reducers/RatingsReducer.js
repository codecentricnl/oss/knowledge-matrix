import _ from "lodash";
import {GET_RATINGS} from "../actions/index";

export default function (state = {}, action) {
    switch (action.type) {
        case GET_RATINGS:
            return _.mapKeys(action.payload.data, 'id');
        default:
            return state;
    }
}