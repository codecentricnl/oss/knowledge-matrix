import {EMPLOYEES} from '../actions'
import _ from "lodash";
import {ADD_EMPLOYEE, EMPLOYEE} from "../actions/index";

export default function (state = {}, action) {
    switch (action.type) {
        case EMPLOYEES:
            return _.mapKeys(action.payload.data, 'id');
        case EMPLOYEE:
            const emp = action.payload.data;

            // map the keys of the skills so these are easily accessible
            if(emp.ratings){
                const formattedRatings = _.mapKeys(emp.ratings, 'skill_id');
                emp.ratings = formattedRatings;
            }
            return {...state, [emp.id]: emp};
        case ADD_EMPLOYEE:
            const new_emp = action.payload.data;
            return {...state, [new_emp.id]: new_emp};
        default:
            return state;
    }
}