import _ from "lodash";
import {ADD_SKILLCOLLECTIONS, GET_SKILLCOLLECTION, GET_SKILLCOLLECTIONS} from "../actions/index";

export default function (state = {}, action) {
    let skillCollection;
    switch (action.type) {
        case GET_SKILLCOLLECTIONS:
            return _.mapKeys(action.payload.data, 'id');

        case GET_SKILLCOLLECTION:
            skillCollection = action.payload.data;
            // map the keys of the skills so these are easily accessible
            if(skillCollection.skills){
                let formattedSkills = _.mapKeys(skillCollection.skills, 'id');
                skillCollection.skills = formattedSkills;
            }
            return {...state, [skillCollection.id]: skillCollection};

        case ADD_SKILLCOLLECTIONS:
            skillCollection = action.payload.data;
            return {...state, [skillCollection.id]: skillCollection};
        default:
            return state;
    }
}