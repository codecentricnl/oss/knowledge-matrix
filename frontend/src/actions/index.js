import axios from 'axios'
// const address =  process.env.REACT_APP_API_URL;
const address = 'http://knowledge-matrix-alb-1145877530.eu-west-1.elb.amazonaws.com';

export const EMPLOYEES = 'employees';
export const EMPLOYEE = 'employee';
export const ADD_EMPLOYEE = 'add_employee';
export const GET_SKILLCOLLECTION = 'get_skillcollection';
export const GET_SKILLCOLLECTIONS = 'get_skillcollections';
export const ADD_SKILLCOLLECTIONS = 'add_skillcollections';
export const SKILLS = 'skills';
export const ADD_SKILL = 'add_skill';
export const DELETE_SKILL = 'delete_skill';
export const GET_RATINGS = 'get_ratings';
export const RATING = 'rating';
export const SET_CURRENT_USER = 'set_current_user';

export function getEmployees() {

    const request = axios.get(`${address}/employees`);

    return {
        type: EMPLOYEES,
        payload: request
    }
}

export function getEmployee(id) {
    const request = axios.get(`${address}/employees/${id}`);

    return {
        type: EMPLOYEE,
        payload: request
    }
}

export function addEmployee(name) {
    const request = axios.post(`${address}/employees`, {name});

    return {
        type: ADD_EMPLOYEE,
        payload: request
    }
}

export function getSkillCollections() {
    const request = axios.get(`${address}/skillcollections`);

    return {
        type: GET_SKILLCOLLECTIONS,
        payload: request
    }
}

export function getSkillCollection(id) {
    const request = axios.get(`${address}/skillcollections/${id}`);

    return {
        type: GET_SKILLCOLLECTION,
        payload: request
    }
}

export function addSkillCollection(name) {
    const request = axios.post(`${address}/skillcollections`, {name});

    return {
        type: ADD_SKILLCOLLECTIONS,
        payload: request
    }
}

export function getSkills() {
    const request = axios.get(`${address}/skills`);

    return {
        type: SKILLS,
        payload: request
    }
}

export function getSkillsByCollectionId(id) {
    const request = axios.get(`${address}/skills/byCollectionId/${id}`);

    return {
        type: SKILLS,
        payload: request
    }
}

export function addSkill(name, skillCollectionId) {
    const request = axios.post(`${address}/skillcollections/${skillCollectionId}`, {name});

    return {
        type: ADD_SKILL,
        payload: request
    }
}

export function deleteSkill(id) {
    const request = axios.delete(`${address}/skills/${id}`);

    return {
        type: DELETE_SKILL,
        payload: request
    }
}

export function updateRating(rating, skill_id, emp_id) {
    const request = axios.post(`${address}/ratings`, {rating, skill_id, emp_id});

    return {
        type: RATING,
        payload: request
    }
}

export const setCurrentUser = (user, token) => {
    localStorage.setItem('currentUser', JSON.stringify(user))
    localStorage.setItem('token', token)
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    return {
        type: SET_CURRENT_USER,
        payload: user
    }
}