import React, {Component} from 'react';
import './App.css';
import '../node_modules/font-awesome/css/font-awesome.css'
import Header from "./components/Header";
import Footer from "./components/Footer";
import UserList from "./components/UserList";
import UserProfile from "./components/UserProfile";
import SkillCollection from "./components/SkillCollection";
import Skills from "./components/Skills";
import Graph from "./components/Graph";
import {Route, Switch} from "react-router-dom";
import axios from "axios";
import Ws from '@adonisjs/websocket-client'

// const ws = Ws('ws://localhost:3333')
// const ws = Ws('ws://knowledge-matrix-alb-1145877530.eu-west-1.elb.amazonaws.com')

class App extends Component {
    constructor(props){
        super(props);

        // const ws = Ws('ws://localhost:3334')
        const ws = Ws('ws://knowledge-matrix-alb-1145877530.eu-west-1.elb.amazonaws.com')
        ws.connect()

        const chat = ws.subscribe('chat')

        chat.on('ready', () => {
          console.log('w r ready')
        })

        chat.on('error', (error) => console.log(error));

        chat.on('message', (userId) => {
            this.props.history.push(`/employee/${userId}`)
        })

    }

    componentWillMount() {
        const token  = localStorage.getItem('token');
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    }

    render() {
        return (
            <div className="App">
                <Header/>
                <div className="container">
                    <Switch>
                        <Route exact path="/" component={Graph}/>
                        <Route exact path="/employee/:id" component={UserProfile}/>
                        <Route path="/employees" component={UserList}/>
                        <Route exact path="/category/:id" component={Skills}/>
                        <Route path="/category" component={SkillCollection}/>
                    </Switch>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default App;
