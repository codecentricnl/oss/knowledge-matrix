import React, {Component} from 'react';
import {Col, Row} from "react-bootstrap";
import Rating from 'react-rating';

class  RatingComponent  extends Component {

    render() {
        const skill = this.props.skill;
        return (
            <Row>
                <Col xs={6}>
                    <p className="pull-right mt-3">{skill.name}</p>
                </Col>
                <Col xs={6}>
                    <div className="pull-left">
                        <Rating
                            empty="fa fa-star-o fa-2x"
                            full="fa fa-star fa-2x"
                            fractions={2}
                            initialRate={this.props.rating}
                            onChange={(rate) => this.props.onClick(rate, skill.id)}
                        />
                    </div>
                </Col>
            </Row>
        );
    }
}

export default RatingComponent;