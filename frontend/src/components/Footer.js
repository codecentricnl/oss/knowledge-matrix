import React, {Component} from 'react';
import {Image} from "react-bootstrap";

class Footer extends Component {
    render() {
        return (
            <div className="navbar navbar-default navbar-fixed-bottom">
                  <Image src="/images/cc.png" style={{width: '200px', display: 'block', margin: '0 auto', marginTop: '15px'}}></Image>
            </div>
        );
    }
}

export default Footer;
