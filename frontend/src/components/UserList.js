import React, {Component} from 'react';
import UserCard from "./UserCard";
import {Link} from "react-router-dom";
import {getEmployees} from '../actions'
import {connect} from "react-redux";
import _ from 'lodash'
import {Button, Col, ControlLabel, FormControl, FormGroup, Modal, Row} from "react-bootstrap";
import {addEmployee} from "../actions/index";

class UserList extends Component {

    constructor(props) {
        super(props);
        this.state = {showModal: false, employeeName: ''};
    }

    componentDidMount() {
        this.props.getEmployees();
    }

    open = () => {
        this.setState({showModal: true});
    };

    close = () => {
        this.setState({showModal: false});
    };

    renderUserCards() {
        if (_.isEmpty(this.props.employees)) {
            return (

                    <Col xs={12}>
                        <p>You have no employees defined.</p>
                    </Col>
            )
        }
        return _.map(this.props.employees, employee => {
            return (
                <Link key={employee.id} to={"/employee/" + employee.id}>
                    <UserCard name={employee.name}/>
                </Link>
            )
        })
    };

    onFormSubmit = (e) => {
        e.preventDefault();
        this.props.addEmployee(this.state.employeeName);
        this.setState({employeeName: ''});
        this.close();
    };

    render() {
        return (
            <div>
                <Row>
                    <Col xs={8}>
                        <h3 className="pull-left">
                            Employees
                        </h3>
                    </Col>
                    <Col xs={4}>
                        <Button className="pull-right mt-17" onClick={() => this.open()}>
                            <i className="fa fa-plus" aria-hidden="true"/> Add employee
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <div>
                        {this.renderUserCards()}
                    </div>
                </Row>

                <Modal show={this.state.showModal}
                       keyboard={true}
                       onHide={this.close}
                       enforceFocus={false}
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Add an employee</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form onSubmit={(e) => this.onFormSubmit(e)}>
                            <FormGroup controlId="formBasicText">
                                <ControlLabel>Employee name</ControlLabel>
                                <FormControl
                                    type="text"
                                    value={this.state.employeeName}
                                    onChange={(e) => this.setState({employeeName: e.target.value})}
                                    autoFocus
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {employees: state.employees};
}

export default connect(mapStateToProps, {getEmployees, addEmployee})(UserList);