import React, {Component} from 'react';
import {Button, Col, ControlLabel, FormControl, FormGroup, Modal, Row} from "react-bootstrap";
import {connect} from "react-redux";
import {addSkill, deleteSkill, getSkillsByCollectionId} from "../actions/index";
import _ from "lodash";

class Skills extends Component {

    constructor(props) {
        super(props);
        this.state = {showModal: false, skillName: ''};
    }

    componentDidMount() {
        this.props.getSkillsByCollectionId(this.props.match.params.id);
    }

    open = () => {
        this.setState({showModal: true});
    };

    close = () => {
        this.setState({showModal: false});
    };

    onFormSubmit = (e) => {
        e.preventDefault();
        this.props.addSkill(this.state.skillName, this.props.match.params.id);
        this.setState({skillName: ''});
        this.close();
    };

    deleteSkill = (id) => {
        this.props.deleteSkill(id);
    };

    renderTableElements = () => {
        return _.map(this.props.skills, skill => {
                return (
                    <tr key={skill.id}>
                        <td>{skill.id}</td>
                        <td>{skill.name}</td>
                        <td>
                            <i className="fa fa-remove" onClick={() => (this.deleteSkill(skill.id))}/>
                        </td>
                    </tr>
                )
            }
        )
    };

    render() {
        return (
            <div>
                <Row>
                    <Col xs={12}>
                        <Row>
                            <Col xs={8}>
                                <h3 className="pull-left">
                                    Skills
                                </h3>
                            </Col>
                            <Col xs={4}>
                                <Button className="pull-right mt-17" onClick={() => this.open()}>
                                    <i className="fa fa-plus"/> Add skill
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                {_.isEmpty(this.props.skills) ?
                        <p>There are no skills defined </p> :
                        <Row>
                            <Col xs={12}>
                                <div className="panel panel-default">
                                    <div className="panel-body">
                                        <table className="table table-striped table-hover ">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th/>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {this.renderTableElements()}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                }

                <Modal show={this.state.showModal}
                       enforceFocus={false}
                       onHide={this.close}
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Add a skill</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form onSubmit={(e)=> this.onFormSubmit(e)}>
                            <FormGroup controlId="formBasicText">
                                <ControlLabel>Skill name</ControlLabel>
                                <FormControl
                                    type="text"
                                    value={this.state.skillName}
                                    placeholder="Enter text"
                                    onChange={(e) => this.setState({skillName: e.target.value})}
                                    autoFocus
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }
}

function mapStateToProps({skills}) {
    return {
        skills: skills
    }
}


export default connect(mapStateToProps, {getSkillsByCollectionId, addSkill, deleteSkill})(Skills);