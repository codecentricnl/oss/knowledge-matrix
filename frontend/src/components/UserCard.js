import React from 'react';
import {Col} from "react-bootstrap";

function UserCard({name}) {
    return (
        <Col xs={4} sm={2}>
            <div className="panel panel-default">
                <img style={{display: 'block', width: '100%', height: 'auto'}}
                     src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif"
                     alt="codecentricFTW!"
                />
                <div className="panel-body">
                    <h5>{name}</h5>
                </div>
            </div>
        </Col>
    );
}

export default UserCard;