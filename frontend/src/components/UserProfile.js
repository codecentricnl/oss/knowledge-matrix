import React, {Component} from 'react';
import {Col, Row} from "react-bootstrap";
import RatingComponent from "./Rating";
import {getEmployee, getSkills, updateRating} from "../actions/index";
import {connect} from "react-redux";
import _ from 'lodash'

class UserProfile extends Component {

    componentDidMount() {
        const emp_id = this.props.match.params.id;
        this.props.getEmployee(emp_id);
        this.props.getSkills();
    }

    onRatingClick = (rating, skill_id) => {
        this.props.updateRating(rating, skill_id, this.props.match.params.id);
    };

    renderSkills() {
        return _.map(this.props.skills, skill => {
            const employee = this.props.employee;
            let rating = 0;
            if(_.has(employee, `ratings[${skill.id}]`)) {
                rating = employee.ratings[skill.id].rate;
            }

            return (
                <RatingComponent
                    key={skill.id}
                    skill={skill}
                    onClick={this.onRatingClick}
                    rating={rating}/>
            )
        });
    }

    render() {
        if (!this.props.employee) {
            return (<div>Loading...</div>);
        }

        if (_.isEmpty(this.props.skills)) {
            return <p> You have no skills defined you can do this here: [enter skill link here]</p>
        }
        return (
            <div>
                <Row>
                    <Col sm={12}>
                        <h3 className="pull-left">Knowledge matrix
                            of {this.props.employee.name}
                        </h3>
                    </Col>
                </Row>

                <Row>
                    <Col sm={12}>
                        <div className="panel panel-default">
                            <div className="panel-body">
                                <Col sm={12}>
                                    {this.renderSkills()}
                                </Col>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>

        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        employee: state.employees[ownProps.match.params.id],
        skills: state.skills
    };
}

export default connect(mapStateToProps, {getEmployee, getSkills, updateRating})(UserProfile);