import React, {Component} from 'react';
import {Button, Col, ControlLabel, FormControl, FormGroup, Modal, Row} from "react-bootstrap";
import {addSkillCollection, getSkillCollections} from "../actions/index";
import {connect} from "react-redux";
import * as _ from "lodash";
import {Link} from "react-router-dom";

class SkillCollection extends Component {

    constructor(props) {
        super(props);
        this.state = {showModal: false, skillCollectionName: ''};
    }

    componentDidMount() {
        this.props.getSkillCollections();
    }

    open = () => {
        this.setState({showModal: true});
    }

    close = () => {
        this.setState({showModal: false});
    }

    onFormSubmit = (e) => {
        e.preventDefault();
        this.props.addSkillCollection(this.state.skillCollectionName);
        this.setState({skillCollectionName: ''});
        this.close();
    }

    renderTableElements = () => {

        if (_.isEmpty(this.props.skillCollections)) {
            return (
                <p>You have no categories defined.</p>
            )
        }
        return (
            <div className="panel panel-default">
                <div className="panel-body">
                    <table className="table table-striped table-hover ">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        {_.map(this.props.skillCollections, skillCollection => {
                                return (
                                    <tr key={skillCollection.id}>
                                        <td>
                                            {skillCollection.id}
                                        </td>
                                        <td>
                                            <Link to={`category/${skillCollection.id}`}>{skillCollection.name}</Link>
                                        </td>
                                    </tr>
                                )
                            }
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
        )


    };

    render() {
        return (
            <div>
                <Row>
                    <Col xs={8}>
                        <h3 className="pull-left">
                            Category
                        </h3>
                    </Col>
                    <Col xs={4}>
                        <Button className="pull-right mt-17" onClick={() => this.open()}>
                            <i className="fa fa-plus" aria-hidden="true"/> Add category
                        </Button>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        {this.renderTableElements()}
                    </Col>
                </Row>
                <Modal show={this.state.showModal}
                       keyboard={true}
                       onHide={this.close}
                       enforceFocus={false}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add a category</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form onSubmit={(e) => this.onFormSubmit(e)}>
                            <FormGroup controlId="formBasicText">
                                <ControlLabel>Category name</ControlLabel>
                                <FormControl
                                    type="text"
                                    value={this.state.skillCollectionName}
                                    onChange={(e) => this.setState({skillCollectionName: e.target.value})}
                                    autoFocus
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        skillCollections: state.skillCollections
    };
}

export default connect(mapStateToProps, {getSkillCollections, addSkillCollection})(SkillCollection);
