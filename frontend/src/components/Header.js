import React from 'react';
import {NavLink} from "react-router-dom";

function Header() {
    return (
        <nav className="navbar navbar-inverse">
            <div className="container-fluid">
                <ul className="nav navbar-nav" style={{fontSize: '1.8rem'}}>
                    <li>
                        <NavLink to={`/`} exact>
                            <i className="fa fa-pie-chart"/> Home
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to={`/employees`}>
                            <i className="fa fa-users"/> Employees
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to={`/category`}>
                            <i className="fa fa-wrench"/> Skills
                        </NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default Header;