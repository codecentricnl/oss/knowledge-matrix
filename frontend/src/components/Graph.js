import React, {Component} from 'react';
import {
    Radar, RadarChart, PolarGrid,
    PolarAngleAxis, PolarRadiusAxis, Tooltip
} from 'recharts';
import _ from 'lodash';
import {getEmployees, getSkillCollections} from "../actions/index";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

class Graph extends Component {

    constructor(props) {
        super(props);
        props.getEmployees();
        props.getSkillCollections();
    }

    createArrayForChart = () => {
        // instantiate array
        const dataForChart = [];

        // loop over all skillCollections
        _.map(this.props.skillCollections, skillCollection => {

            let ratingSum = 0;

            // loop over all skills
            _.map(skillCollection.skills, skill => {

                // loop over all ratings
                _.map(skill.ratings, rating => {

                    // create sum of all ratings for this skillCollection
                    ratingSum += rating.rate;

                });
            });

            // calculate averages
            let avg = ratingSum / _.size(skillCollection.skills) / _.size(this.props.employees);
            dataForChart.push({name: skillCollection.name, rate: avg});

        });

        return dataForChart;
    };


    render() {
        if (_.isEmpty(this.props.skillCollections)) {
            return (
                <div>
                    <p className="text-center">There is not enough data to render a graph.</p>
                    <p className="text-center">You can add a category&nbsp;
                        <Link to="/skillcollections">
                            here
                        </Link>
                    </p>
                    <p className="text-center">You can add a employee&nbsp;
                        <Link to="/employees">
                            here
                        </Link>
                    </p>
                </div>
            )

        }

        return (
            <div className="text-center">
                <RadarChart outerRadius={200} width={1170} height={500} data={this.createArrayForChart()}>
                    <Radar name="average" dataKey="rate" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6}/>
                    <PolarGrid />
                    <PolarAngleAxis ticks={5} dataKey="name"/>
                    <PolarRadiusAxis domain={[0, 5]}/>
                    <Tooltip/>
                </RadarChart>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return state;
}
export default connect(mapStateToProps, {getEmployees, getSkillCollections})(Graph);