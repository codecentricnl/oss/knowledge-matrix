import React, {Component} from 'react';
import {GoogleLogin} from 'react-google-login';
import axios from 'axios'
import {getEmployees, getSkillCollections} from "../actions";
import {connect} from "react-redux";

const address = 'http://knowledge-matrix-alb-1145877530.eu-west-1.elb.amazonaws.com';
// const address =  process.env.REACT_APP_API_URL;

import {setCurrentUser} from "../actions/index";

const backgroundUrl = 'https://www.codecentric.nl/files/2017/11/DJI_0008.jpg';

class Login extends Component {
    responseGoogle = async (response) => {
        console.log(response);
        try {
            axios.defaults.headers.common['Authorization'] = `Bearer ${response.tokenId}`;
            const result = await axios.post(`${address}/auth`, response.profileObj)
            setCurrentUser(result.data, response.tokenId);
            this.props.history.push('/');
        } catch (e) {
            console.log(e)
        }
    };

    render() {
        return (
            <div style={{
                backgroundImage: `url(${backgroundUrl})`,
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                minHeight: '100vh',
                margin: 0
            }}>
                <div className="modal" style={{display: 'block'}}>
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-body">
                                <h1 className={'text-center'}>codecentric knowledge-matrix</h1>
                                <div className={'text-center'}>
                                    <GoogleLogin
                                        clientId="677021621087-1ukfiihps41evonpkacvu57qkmrnr1v4.apps.googleusercontent.com"
                                        buttonText="Login"
                                        onSuccess={this.responseGoogle}
                                        onFailure={this.responseGoogle}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({});
export default connect(mapStateToProps, {getEmployees, getSkillCollections})(Login);