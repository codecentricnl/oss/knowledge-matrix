import "babel-polyfill";
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import {applyMiddleware, createStore} from "redux";
import promise from 'redux-promise';
import {Provider} from "react-redux";
import reducers from './reducers';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Login from "./components/Login";
import {PrivateRoute} from "./components/PrivateRoute";

const createStoreWithMiddleWare = applyMiddleware(promise)(createStore);

ReactDOM.render(
    <Provider store={createStoreWithMiddleWare(reducers)}>
        <Router>
            <Switch>
                <Route path={"/login"} component={Login}/>
                <PrivateRoute path={"/"} component={App}/>
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root')
);
