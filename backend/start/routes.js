'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route');

Route.group(() => {
    Route.get('/employees', 'EmployeesController.index');
    Route.get('/employees/:id', 'EmployeesController.show');
    Route.post('/employees', 'EmployeesController.store');

    Route.get('/skills', 'SkillsController.index');
    Route.get('/skills/byCollectionId/:id', 'SkillsController.getByCollectionId');
    Route.post('/skills', 'SkillsController.store');
    Route.delete('/skills/:id', 'SkillsController.delete');

    Route.get('/ratings/employee/:employee_id', 'RatingsController.index');
    Route.get('/ratings', 'RatingsController.index');
    Route.post('/ratings', 'RatingsController.store');

    Route.get('/skillcollections', 'SkillCollectionController.index');
    Route.get('/skillcollections/:id', 'SkillCollectionController.index');
    Route.post('/skillcollections', 'SkillCollectionController.store');
    Route.post('/skillcollections/:id', 'SkillCollectionController.storeSkill');
  })
  .middleware(['gauth'])

Route.get('/findBestBySkill/:skill', 'AlexaController.findBestBySkill');
Route.get('/showProfile/:name', 'AlexaController.showProfile');

Route.post('/auth', 'UserController.auth').middleware(['gauth']);
