'use strict';
const { OAuth2Client } = require("google-auth-library");
const User = use('App/Models/User')
const AuthException = use('App/Exceptions/AuthException')
const _ = require('lodash')

class Gauth {
  async handle ({ request }, next) {

    const completeToken = request.header('Authorization')
    if(_.isUndefined(completeToken)) {
      throw new AuthException()
    }
    const idToken = completeToken.substring(7);
    const audience = "677021621087-1ukfiihps41evonpkacvu57qkmrnr1v4.apps.googleusercontent.com";
    const client = new OAuth2Client(audience, '', '');

    // Specify the CLIENT_ID of the app that accesses the backend
    const {payload} = await client.verifyIdToken({idToken, audience})
    request.user = await User.findBy('email', payload.email);

    // call next to advance the request
    await next()
  }
}

module.exports = Gauth
