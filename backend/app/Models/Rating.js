'use strict';

const Model = use('Model')

class Rating extends Model {

  Employee () {
    return this.belongsTo('App/Models/Employee')
  }

  Skill () {
    return this.belongsTo('App/Models/Skill')
  }
}

module.exports = Rating;
