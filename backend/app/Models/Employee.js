'use strict'

const Model = use('Model')

class Employee extends Model {

  ratings() {
    return this.hasMany('App/Models/Rating')
  }
}

module.exports = Employee;
