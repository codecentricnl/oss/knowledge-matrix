'use strict';

const Model = use('Model')

class Skill extends Model {

  skillCollection() {
    return this.belongsTo('App/Models/SkillCollection');
  }

  ratings() {
    return this.hasMany('App/Models/Rating')
  }
}

module.exports = Skill;
