'use strict'

const Model = use('Model')

class SkillCollection extends Model {

  skills () {
    return this.hasMany('App/Models/Skill');
  }
}

module.exports = SkillCollection;
