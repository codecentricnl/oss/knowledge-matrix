'use strict'
const Event = use('Event')

class ChatController {
  constructor ({ socket, request }) {
    console.log(`connected with ${socket} and ${request}`)
    this.socket = socket
    this.request = request
    Event.on('ws:changeRoute', (id) => this.socket.broadcastToAll('message', id));
  }

  onMessage (message) {
     this.socket.broadcastToAll('message', message)
  }
}

module.exports = ChatController
