'use strict'

const Skill = use('App/Models/Skill');
const Employee = use('App/Models/Employee');
const Database = use('Database');
const Event = use('Event')

class AlexaController {
  async findBestBySkill({params}) {
    const skill = await Skill.findBy('name', params.skill);
    const bestRating = await Database.table('ratings').where('skill_id', skill.id).orderBy('rate', 'desc').first();
    const employee = await Employee.find(bestRating.employee_id);
    return employee.name;
  }

  async showProfile({params}) {
    try {
      const employee = await Employee.findBy('name', params.name);
      Event.emit('ws:changeRoute', employee.id)
      return employee;
    }
    catch(e) {
      console.log(e)
      return e;
    }

  }
}

module.exports = AlexaController;
