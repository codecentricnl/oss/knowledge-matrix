'use strict'

const Skill = use('App/Models/Skill');


class SkillsController {
  async index(req, res) {
    return await Skill.all();
  }

  async store(req, res) {
    const skill = new Skill();
    skill.name = req.input('name');
    await skill.save();
    return skill
  }

  async delete({params}, res) {
    const skill = await Skill.findBy('id', params.id);
    await skill.delete();
    return skill
  }

  async getByCollectionId({params}, res) {
    const skills = await Skill.query().where('skill_collection_id', params.id);
    return skills
  }
}

module.exports = SkillsController;
