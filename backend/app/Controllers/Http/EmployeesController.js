'use strict';

const Employee = use('App/Models/Employee');
const Rating = use('App/Models/Rating');
const Skill = use('App/Models/Skill');
const _ = require('lodash');

class EmployeesController {

  async index(req, res) {
    return await Employee.all();
  }

  async show({params}, res) {
    const id = params.id;

    const employee = await Employee
      .query()
      .where('id', id)
      .with('ratings')
      .first();

    return employee
  }

  async store({request}) {
    const employee = new Employee();
    employee.name = request.input('name');
    await employee.save();
    return employee
  }
}

module.exports = EmployeesController;
