'use strict'

const SkillCollection = use('App/Models/SkillCollection');
const Skill = use('App/Models/Skill');

class SkillCollectionController {

  async index ({params}) {
    const skillCollectionId = params.id;

    let skillCollection;
    if(skillCollectionId) {
      skillCollection = await SkillCollection.query().where('id', skillCollectionId).with('skills').first();
    } else {
      skillCollection = await SkillCollection.query().with('skills.ratings').fetch();
    }
    return skillCollection
  }

  async store ({request}, res) {
    const skillCollection = new SkillCollection();
    skillCollection.name = request.input('name');
    await skillCollection.save();
    return skillCollection
  }

  async storeSkill ({params, request}, res) {
    const skillCollection = await SkillCollection.find(params.id);

    const skill = new Skill();
    skill.name = request.input('name');
    await skillCollection.skills().save(skill);
    return skill
  }
}

module.exports = SkillCollectionController;
