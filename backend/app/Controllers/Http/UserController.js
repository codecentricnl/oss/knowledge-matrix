const User = use('App/Models/User')

class UserController {

  async auth({request, response, auth}) {
    // Check if we need to register or login.
    const {email, googleId, imageUrl, name} = request.all()
    let user;
    try {
      // if this succeeds, we can assume that Gauth will do validations on different endpoints
      user = await User.findByOrFail('email', email);
    } catch (e) {
      user = new User()
      user.email = email
      user.name = name
      user.googleId = googleId
      user.imageUrl = imageUrl

      await user.save();
    }
    return user;
  }
}

module.exports = UserController;

