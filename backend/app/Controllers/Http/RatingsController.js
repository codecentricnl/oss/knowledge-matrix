'use strict';

const Rating = use('App/Models/Rating');
const Employee = use('App/Models/Employee');
const Skill = use('App/Models/Skill');


class RatingsController {

  async index({params}, res) {
    // get ratings for an employee
    const employee_id = params.employee_id;

    let ratings;
    if(employee_id) {
      ratings = await Rating.query().where('employee_id', employee_id)
    } else {
      ratings = await Rating.all();
    }
    return ratings
  }

  async store({request}, res) {

    // see if the rating is already there so we can just easily update it.
    const employee = await Employee.find(request.input('emp_id'));
    const skill = await Skill.find(request.input('skill_id'));

    // findOrCreate always returns the existing created object.
    // in case of existing, the rating still needs to be changed
    const rating = await Rating.findOrCreate(
      { employee_id: employee.id, skill_id: skill.id },
      { employee_id: employee.id, skill_id: skill.id }
    );

    // change the rating
    rating.rate = request.input('rating');
    await rating.save();

    return rating
  }
}

module.exports = RatingsController;
