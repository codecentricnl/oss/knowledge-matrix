'use strict'

const Schema = use('Schema')

class EmployeesTableSchema extends Schema {

  up () {
    this.create('employees', (table) => {
      table.increments()
      table.string('name')
      table.timestamps()
    })
  }

  down () {
    this.drop('employees')
  }

}

module.exports = EmployeesTableSchema
