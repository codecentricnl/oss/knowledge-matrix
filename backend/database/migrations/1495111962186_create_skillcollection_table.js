'use strict'

const Schema = use('Schema')

class SkillCollectionsTableSchema extends Schema {

  up () {
    this.create('skill_collections', (table) => {
      table.increments()
      table.string('name')
      table.timestamps()
    })
  }

  down () {
    this.drop('skill_collections')
  }

}

module.exports = SkillCollectionsTableSchema
