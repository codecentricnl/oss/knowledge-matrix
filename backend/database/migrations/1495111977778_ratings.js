'use strict'

const Schema = use('Schema')

class RatingsTableSchema extends Schema {

  up () {
    this.create('ratings', (table) => {
      table.increments();
      table.integer('employee_id').unsigned();
      table.foreign('employee_id').references('id').inTable('employees');
      table.integer('skill_id').unsigned();
      table.foreign('skill_id').references('id').inTable('skills');
      table.decimal('rate');
      table.timestamps()
    })
  }

  down () {
    this.drop('ratings')
  }

}

module.exports = RatingsTableSchema;
