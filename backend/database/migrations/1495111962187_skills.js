'use strict'

const Schema = use('Schema')

class SkillsTableSchema extends Schema {

  up () {
    this.create('skills', (table) => {
      table.increments()
      table.string('name')
      table.integer('skill_collection_id').unsigned();
      table.foreign('skill_collection_id').references('id').inTable('skill_collections');
      table.timestamps()
    })
  }

  down () {
    this.drop('skills')
  }

}

module.exports = SkillsTableSchema
